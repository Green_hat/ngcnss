import {Component} from '@angular/core';
import {FormationService} from './formation.service';
import {Assure} from './assure';
import {findAll} from '@angular/compiler-cli/src/ngcc/src/utils';

@Component({
  selector: 'app-cnss',
  styleUrls: ['./cnss.component.css'],
  templateUrl: './cnss.component.html'
})

export class CnssComponent {
  public liste;
  constructor(private formationService: FormationService) { }
  findAll() {
    this.formationService.getAssures()
    .subscribe(response => {
      console.log(response);
      this.liste = response; });
  }
  delete(id) {
    this.formationService.delete(id).subscribe(a => console.log(a) );
    this.findAll();
  }

  create() {
    const assure = new Assure( 'oussema', '007');
    this.formationService.create(assure).subscribe(a => console.log(a) );
    this.findAll();
  }


}
