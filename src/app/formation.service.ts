import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Assure} from './assure';
import {Observable} from 'rxjs';
@Injectable()
export class FormationService {
//  public assures;
 constructor(private http: HttpClient) { }

 getAssures(): Observable<Assure> {
   return this.http.get<Assure>('http://127.0.0.1:8080/assure');
  }

  delete(id: number) {
    return this.http.delete('http://127.0.0.1:8080/assure/' + id);
  }
  create(assure: Assure): Observable<Assure> {
    return this.http.post<Assure>('http://127.0.0.1:8080/assure', assure);
  }
}
