import { Component } from '@angular/core';
import {Assure} from '../assure';
import {FormationService} from '../formation.service';

@Component({
  selector: 'app-assure',
  templateUrl: './assure.component.html',
  styleUrls: ['./assure.component.css']
})
export class  AssureComponent {
  assure: Assure = new Assure('', '', 0);
  constructor(private formationService: FormationService) {  }
  creer() {
    this.formationService.create(this.assure).subscribe(assureResp => this.assure = assureResp);
  }
}


