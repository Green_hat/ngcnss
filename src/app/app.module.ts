import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {CnssComponent} from './cnss.component';
import { AssureComponent } from './assure/assure.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {FormationService} from './formation.service';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    CnssComponent,
    AssureComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [FormationService, HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
